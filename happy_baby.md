---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: "Happy Baby : Application Mobile" 
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
attribué à : 
  - Cedric Mujynya  
professeurs co-superviseurs:
  - Non Attribué
mots-clé:
  - Baby sittings 
  - Application Mobile
langue: [F,E]
confidentialité: non
suite: non
---
\begin{center}
\includegraphics[width=0.3\textwidth]{images/smartphone.png}
\end{center}

## Description
l'idée est de développer une application mobile permettant de mettre en contacts des parents cherchant une nounou pour s’occuper de leur enfant.  
Pour ce faire, l’application sera un mix entre Uber et Tinder :  
Le parent peut voir les mamans de jours/nounous proches de sa localisation (sans pour autant connaître la localisation précise de la personne, données privées): Similaire à Uber.  
Si le parent est interessé par le profil de la personne, il peut envoyer un "like" indiquant que le parent souhaite prendre contact. La nounou peut alors voir les informations de base de/des enfant(s) à garder et décider si oui ou non elle accepte la conversation: Similaire à Tinder

## Buts
Comme cette application sera développée, testée et finalisée dans un travail de bachelor. Le but du projet de semestre 6 est de définir les besoins, analyser l'état du marché, concevoir et développer un POC (Proof fo concept) afin de lancer une série de tests utilisateurs et de mesurer l'acceptation du produit.

## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Analyse de l'applications existance et d'autres, ainsi des technologies possibles pour cette applications au niveua des plateformes de développement (Android, iOS, Xamarin, Flutter)
- Conception d'un POC (Proof of concept)
- Implementation du POC (application mobile)
- Tests et validation du système.
- Rédaction d'une documentation adéquate.
