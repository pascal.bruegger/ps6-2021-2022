---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: Insurance Sim Card Reader - Lecture de carte SIM d'assurance maladie
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - SoftDesign Sàrl
professeurs co-superviseurs:
  - Non Attribué
mots-clé: 
  - SIM Card
  - Assurance maladie
  - Cabinet médical
  - Lecteur de carte]
langue: [F,E]
confidentialité: non
suite: non
---

\begin{center}
\includegraphics[width=0.25\textwidth]{images/cherry.png}
\end{center}

## Description
En Suisse, l’assurance maladie étant obligatoire, nous possédons tous une carte qui donne accès à nos informations personnelles (nom, prénom, no AVS, etc.). Dans bien des cabinets médicaux, le patient doit soit passer sa carte en lecture magnétique soit l'insérer dans un lecteur de carte SIM afin de récupérer ces informations. Commnent les données personnelles sont-elles stockées au niveau de la SIM? La carte contient-elle uniquement un numéro qui donne accès aux données grâce au serveur CoverCard (comme dans le cas d'une lecture magnétique)? Comment les accède-t-on? Quels sont les systèmes de sécurités pour ces données? 
Une fois ces éléments déterminés, le projet consiste à intégrer dans un logiciel de gestion de cabinets de physio/ostéo la lecture de la carte afin de permettre la création d’un nouveau patient ou sa mise à jour. Le logiciel en question s’appelle TurboPhysio et permet la gestion de cabinet de physiothérapie (Factureation et prise de rendez-vous, etc.). Ce logiciel a été développé avec le langage objet Delphi, sur la plateforme de développement Embarcadero.

## Contraintes (négociables)
  - 2 contraintes sont à tenir en compte:
  - Le lecteur de SIM card qu'il faudra étudier est le Cherry ST-1044U (un des plus répandu)
  - L'intégration du résultat de la lecture devra se faire en Delphi et l'IDE Embarcadero avec l'application TurboPhysio

## Objectifs/Tâches
  - Etudier la ou les technologies qui se cache(nt) derrières les cartes SIM (Données, sécurité, protocoles, etc.) et en particulier la carte d'assurance maladie.
  - Etudier des applications capables de lire les cartes SIM.
  - Etudier quelques concepts du langage objet Delphi.
  - Intégrer les données lues dans dans l'application de gestion (essentiellement dans la vue "Fiche Patient").
  - Tests et validation du système.
  - Rédaction d'une documentation adéquate

 L'étudiant recevra le lecteur Cherry et une machine avec l'IDE Embarcadero sur lequel il pourra développer la partie logiciel.

