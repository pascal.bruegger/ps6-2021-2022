---
version: 2
type de projet: Projet de semestre 6
année scolaire: 2021/2022
titre: TurboPhysio Mobile II - Agenda Mobile pour therapeute  
filières:
  - Informatique
  - Télécommunications
nombre d'étudiants: 1
mandants:
  - SoftDesign SArl
professeurs co-superviseurs:
  - Non Attribué
mots-clé:
  - Agenda
  - Scheduler
  - Application Mobile
  - Flutter
langue: [F,E]
confidentialité: non
suite: non
---
\begin{center}
\includegraphics[width=0.3\textwidth]{images/smartphone.png}
\end{center}

## Description
TurboPhysio est un logiciel qui permet de gérer toutes les interventions et la facturation d’un cabinet de physiothérapie et d’ergothérapie. Actuellement, la mise à jour du dossier patient et la facturation ne se fait que par une station (laptop ou desktop) soit sur un serveur soit sur le cloud (accès aux bases de données) ou en stand-alone.
Ce logiciel offre aussi un agenda connecté à la base de donnée et permet de prendre des rendez-vous pour les patients. Les thérapeutes peuvent le consulter sur leur pc afin de gérer leur patient journalier. Le prestataire aimerait offrir aux cabinets la possibilité d'avoir cette fonctionalité sur smartphone et tablette Android et iOS. Une version de l'application existe déjà en Xamarin. 

## Buts
Le but du projet est de proposer une application mobile pour la prise de rendez-vous des physio-ergothérapeute sur smartphone/tablette afin de rendre le système plus souple et permettre, par exemple lors d’intervention à domicile.
La contrainte principale est le développement en Flutter afin que l'application soit cross-plateforme.

L’étudiant devra étudier la possibilité d'avoir la prise de rendez-vous de manière off-line (par ex BD locale et es mises à jour des bases de données dans les 2 sens (mobile -> serveur et serveur -> mobile).

Enfin, une attention particulière sera sur l’aspect ergonomique de l’interface graphique.

## Objectifs/Tâches

- Elaboration d'un cahier des charges
- Analyse de l'applications existance et d'autres, ainsi des technologies possibles pour cette applications au niveau des plateformes de développement (Android, iOS, Xamarin, Flutter)
- Conception 
- Implementation de l'app
- Tests et validation du système.
- Rédaction d'une documentation adéquate.
